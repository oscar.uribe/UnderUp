package com.underup.web;

import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.underup.service.SampleService;

@RunWith(MockitoJUnitRunner.class)
public class SampleControllerTest {
	
	@Mock
	private SampleService sampleService;
	
	@InjectMocks
	private SampleController sampleController;

	@Before
	public void init() {
		
	}
	
	@Test
	public void test() {
		
		when(sampleService.welcome("Mauricio")).thenReturn("Hola Mauricio");
		String mensaje = sampleController.welcome("Mauricio");
		
		assertEquals("Hola Mauricio", mensaje);
		
		int a[] = {5,3,4,5};
		
	}

}
