package com.underup.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.underup.model.Producto;
import com.underup.service.ProductoService;

@RestController
public class ProductoController {
	
	@Autowired
	ProductoService productoService;
	
	@PostMapping(value="/producto")
	public Producto registrarProducto(@RequestBody Producto producto) {
		
		return productoService.registrarProducto(producto);		
	}
	
	@GetMapping("")
	public List<Producto> get() {
		return productoService.get();
	}

}
