package com.underup.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.underup.model.Producto;

public interface IProductoRepository extends CrudRepository<Producto, Long> {
	
	public  List<Producto> findByTipo(String tipo);

}
