package com.underup.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.underup.model.Producto;
import com.underup.repository.IProductoRepository;

@Service
public class ProductoService {
	
	@Autowired
	IProductoRepository productoRepository;
	
	public Producto registrarProducto(Producto producto) {
		
		return productoRepository.save(producto);
	}
	
	public List<Producto> get() {
		
		return (List<Producto>) productoRepository.findAll();
	}
	
	

}
