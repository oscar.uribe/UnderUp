package com.underup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnderUpApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnderUpApplication.class, args);
	}
}
