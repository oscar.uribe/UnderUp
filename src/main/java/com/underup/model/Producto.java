package com.underup.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "producto")
@SequenceGenerator(name = "seq_id_producto", sequenceName = "seq_id_producto", initialValue = 1, allocationSize = 1)
public class Producto implements Serializable{
	
	private static final long serialVersionUID = 8659022279064113856L;

	@Id
	@Column(name = "referencia_prod")
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_id_producto")
	private Long idProducto;
	
	@Column(name = "tipo_prod", nullable = false)
	private String tipo;
	
	@Column(name = "descripcion_prod", nullable = true)
	private String descripcion;
	
	@Column(name = "precioventa_prod", nullable = false)
	private int precioVenta;
	
	public Producto() {
		
	}

	public Producto(@NotNull Long idProducto, String tipo, String descripcion, int precioVenta) {
		super();
		this.idProducto = idProducto;
		this.tipo = tipo;
		this.descripcion = descripcion;
		this.precioVenta = precioVenta;
	}

	public Long getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(int precioVenta) {
		this.precioVenta = precioVenta;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
