package com.underup.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "stock")
@SequenceGenerator(name = "seq_id_stock", sequenceName = "seq_id_stock", initialValue = 1, allocationSize = 1)
public class Stock implements Serializable{
	
	private static final long serialVersionUID = 5515469844002803761L;

	@Id
	@Column(name = "id_stock")
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_id_stock")
	private Long idStock;
	
	@ManyToOne
	@JoinColumn(name = "referencia_prod")
	private Producto producto;
	
	@Column(name = "talla_stock")
	private String talla;
	
	@Column(name = "cantidad_stock")
	private int cantidad;

}
